(defsystem synacor
  :description "Virtual machine for the synacor challenges"
  :depends-on ("uiop")
  :pathname "src/"
  :serial t
  :components
  ((:file "package")
   (:file "conditions")
   (:file "vm")
   (:file "ops")
   (:file "disassemble")
   (:file "debug")
   (:file "challenge")))
