;;;; ops.lisp -- VM instruction set implementation

(in-package #:synacor)

(defvar *instruction-hook* nil
  "Either nil or a function of one argument: the virtual machine state.
This function is invoked before each instruction is executed.")

(defmacro defop ((name opcode) (arity) &body body)
  `(progn (setf (fdefinition ',name)
                (lambda ()
                  (when *instruction-hook*
                    (funcall *instruction-hook* ',name ,opcode))
                  (progn ,@body)))
          (setf (aref *opcode-table* ,opcode) ',name)
          (setf (getf (symbol-plist ',name) :arity) ,arity)))

(defun opcode-mnemonic (opcode)
  (unless (>= opcode +opcode-table-size+)
    (aref *opcode-table* opcode)))

(defun instruction-arity (mnemonic)
  (getf (symbol-plist mnemonic) :arity))

(defop (halt 0) (0)
  (signal 'halt))

(defop (set 1) (2)
  (let ((register (fetch :as :register))
        (value (fetch :as :value)))
    (setf (register-value register)
          (the (unsigned-byte 16) value))))

(defop (push 2) (1)
  (cl:push (fetch :as :value) *stack*))

(defop (pop 3) (1)
  (unless *stack*
    (error 'stack-underflow))
  (let ((register (fetch :as :register)))
    (setf (register-value register)
          (cl:pop *stack*))))

(macrolet ((defbinop ((name opcode) pred)
             (let ((register (gensym))
                   (value1 (gensym))
                   (value2 (gensym)))
               `(defop (,name ,opcode) (3)
                  (let ((,register (fetch :as :register))
                        (,value1 (fetch :as :value))
                        (,value2 (fetch :as :value)))
                    (setf (register-value ,register)
                          (if (funcall #',pred ,value1 ,value2)
                              (the (unsigned-byte 16) 1)
                              (the (unsigned-byte 16) 0))))))))
  (defbinop (eq 4) =)
  (defbinop (gt 5) >))

(defop (jmp 6) (1)
  (setf *ip* (fetch :as :value)))

(defop (jt 7) (2)
  (let ((a (fetch :as :value))
        (b (fetch :as :value)))
    (when (cl:not (zerop a))
      (setf *ip* b))))

(defop (jf 8) (2)
  (let ((a (fetch :as :value))
        (b (fetch :as :value)))
    (when (zerop a)
      (setf *ip* b))))

(declaim (inline op-16bit +-16bit *-16bit))

(defun op-16bit (f x y)
  (declare (type (unsigned-byte 16) x y))
  (the (unsigned-byte 16)
       (cl:mod (funcall f x y) #x8000)))

(defun +-16bit (x y)
  (declare (type (unsigned-byte 16) x y))
  (the (unsigned-byte 16)
       (op-16bit #'+ x y)))

(defun *-16bit (x y)
  (declare (type (unsigned-byte 16) x y))
  (the (unsigned-byte 16)
       (op-16bit #'* x y)))

(macrolet ((def-op-set ((name opcode) op)
             (let ((a (gensym))
                   (b (gensym))
                   (c (gensym)))
               `(defop (,name ,opcode) (3)
                  (let ((,a (fetch :as :register))
                        (,b (fetch :as :value))
                        (,c (fetch :as :value)))
                    (setf (register-value ,a)
                          (the (unsigned-byte 16)
                               (funcall #',op ,b ,c))))))))
  (def-op-set (add 9) +-16bit)
  (def-op-set (mult 10) *-16bit)
  (def-op-set (mod 11) cl:mod)
  (def-op-set (and 12) logand)
  (def-op-set (or 13) logior))

(defop (not 14) (2)
  (let ((a (fetch :as :register))
        (b (fetch :as :value)))
    (setf (register-value a)
          (the (unsigned-byte 16)
               (cl:mod (lognot b) #x8000)))))

(defop (rmem 15) (2)
  (let ((a (fetch :as :register))
        (b (fetch :as :value)))
    (setf (register-value a)
          (memory-value b))))

(defop (wmem 16) (2)
  (let ((a (fetch :as :value))
        (b (fetch :as :value)))
    (setf (memory-value a) b)))

(defop (call 17) (1)
  (let ((a (fetch :as :value)))
    (cl:push *ip* *stack*)
    (setf *ip* a)))

(defop (ret 18) (0)
  (if (null *stack*)
      (signal 'halt)
      (setf *ip* (cl:pop *stack*))))

(defop (out 19) (1)
  (let ((a (fetch :as :value)))
    (princ (code-char a) *vm-output-stream*)))

(defop (in 20) (1)
  (let ((a (fetch :as :register)))
    (let ((char (read-char)))
      (setf (register-value a)
            (char-code char)))))

(defop (noop 21) (0))
