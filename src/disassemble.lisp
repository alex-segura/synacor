;;;; disassemble.lisp -- VM bytecode disassembly and analysis

(in-package #:synacor)

(defclass disassembled-object ()
  ((location
    :type (unsigned-byte 16)
    :initarg :location
    :reader location
    :documentation "Offset into the binary at which this object was parsed."))
  (:documentation "Base class for objects produced during disassembly"))

(defclass operand (disassembled-object)
  ()
  (:documentation "Argument to an instruction."))

(defclass register (operand)
  ((index
    :type (unsigned-byte 16)
    :initarg :index
    :accessor register-index
    :documentation "Index in the register array"))
  (:documentation "Register operand."))

(defmethod print-object ((register register) stream)
  (format stream "R~d" (register-index register)))

(defclass literal (operand)
  ((value
    :type (unsigned-byte 16)
    :initarg :value
    :reader literal-value
    :documentation "Value of the literal datum."))
  (:documentation "Literal operand."))

(defmethod print-object ((literal literal) stream)
  (format stream "~4,'0x" (literal-value literal)))

(defclass data (disassembled-object)
  ((value
    :type (unsigned-byte 16)
    :initarg :value
    :accessor data-value
    :documentation "Representation of a data word."))
  (:documentation "Non-instruction elements discovered during an analysis can be treated
as binary data."))

(defmethod print-object ((data data) stream)
  (format stream "~4,'0x" (data-value data)))

(defclass instruction (disassembled-object)
  ((mnemonic
    :type symbol
    :initform nil
    :initarg :mnemonic
    :accessor instruction-mnemonic
    :documentation "Human-understandable name for the instruction.")
   (opcode
    :type (unsigned-byte 16)
    :initform #x0000
    :initarg :opcode
    :accessor instruction-opcode
    :documentation "16-bit opcode for the instruction.")
   (operands
    :type list
    :initform nil
    :initarg :operands
    :accessor instruction-operands
    :documentation "Arguments manipulated by the instruction."))
  (:documentation "Representation of a VM instruction."))

(defmethod print-object ((instruction instruction) stream)
  (format stream "~4,'0x: ~a~{ ~a~}"
          (location instruction)
          (instruction-mnemonic instruction)
          (instruction-operands instruction)))

(define-condition disassembly-error (simple-error)
  ((location
    :initarg :location
    :reader location))
  (:documentation "Base type for disassembly errors."))

(define-condition invalid-opcode (disassembly-error)
  ((opcode
    :initarg :opcode
    :reader invalid-opcode))
  (:documentation "Signalled when an invalid opcode is detected.")
  (:report (lambda (condition stream)
             (format stream "Invalid opcode #x~4,'0x at ~a."
                     (invalid-opcode condition)
                     (location condition)))))

(define-condition invalid-operand (disassembly-error)
  ((word
    :initarg :word
    :reader invalid-word))
  (:documentation "Signalled when an illegal operand is detected.")
  (:report (lambda (condition stream)
             (format stream "Invalid operand #x~4,'0x at ~a."
                     (invalid-word condition)
                     (location condition)))))

(defclass disassembly ()
  ((data
    :type simple-array
    :initarg :data
    :documentation "Raw bytes from whence the disassembly was produced."
    :accessor disassembly-data)
   (position
    :type integer
    :initarg :position
    :accessor disassembly-position
    :documentation "Position of the current state of the disassembly.")
   (instructions
    :type vector
    :initform (vector)
    :initarg :instructions
    :accessor disassembly-instructions
    :documentation "Vector of disassembled instructions.")
   (instruction-count
    :type integer
    :initarg :instruction-count
    :accessor disassembly-instruction-count
    :documentation "Number of instructions currently disassembled.")
   (code-references
    :type list
    :initform nil
    :initarg :code-references
    :accessor disassembly-code-references
    :documentation "List of references to code elements")
   (data-references
    :type list
    :initform nil
    :initarg :data-references
    :accessor disassembly-data-references
    :documentation "List of references to data delements"))
  (:default-initargs :instruction-count 0
    :code-references nil
    :data-references nil)
  (:documentation "State of a disassembly task."))

(defgeneric disassemble-operand (disassembly)
  (:documentation "Decode the next operand. Updates the disassembly state.")
  (:method ((disassembly disassembly))
    (with-accessors ((position disassembly-position)
                     (data disassembly-data))
        disassembly
      (let* ((location position)
             (word (aref data location)))
        (prog1
            (cond ((literal-value-p word)
                   (make-instance 'literal
                                  :value word
                                  :location location))
                  ((register-p word)
                   (make-instance 'register
                                  :index (decode-register word)
                                  :location location))
                  (t (error 'invalid-operand
                            :word word
                            :location location)))
          (incf position))))))

(defgeneric disassemble-instruction (disassembly)
  (:documentation "Decode the next instruction, updating the disassembly state.")
  (:method ((disassembly disassembly))
    (with-accessors ((data disassembly-data)
                     (position disassembly-position))
        disassembly
      (let* ((location position)
             (opcode (aref data position))
             (name (opcode-mnemonic opcode)))
        (restart-case
            (progn
              (unless name
                (error 'invalid-opcode
                       :opcode opcode
                       :location position))
              (incf position)
              (let* ((arity (instruction-arity name))
                     (rands (loop repeat arity
                               collect (disassemble-operand disassembly))))
                (make-instance 'instruction
                               :mnemonic name
                               :location location
                               :opcode opcode
                               :operands rands)))
          (skip ()
            :report "Continue disassembly attempt at the next byte."
            (prog1 (make-instance 'instruction
                                  :mnemonic 'invalid
                                  :opcode opcode
                                  :location position)
              (incf position)))
          (treat-as-data ()
            :report "Treat this byte as data."
            (prog1 (make-instance 'data
                                  :value opcode
                                  :location position)
              (incf position))))))))

(defun make-disassembly (data &key (position 0))
  (make-instance 'disassembly
                 :data data
                 :position position
                 :instructions (make-array 0 :adjustable t :fill-pointer t)
                 :instruction-count 0))

(defun disassemble (bytes &key (start 0) &aux (end (length bytes)))
  (let ((disassembly (make-disassembly bytes :position start)))
    (with-accessors ((position disassembly-position)
                     (instructions disassembly-instructions)
                     (instruction-count disassembly-instruction-count))
        disassembly
      (restart-case
          (do ()
              ((>= position end))
            (let ((object (disassemble-instruction disassembly)))
              (when (typep object 'instruction)
                (vector-push-extend object instructions)
                (incf instruction-count))))
        (accept ()
          :report "Accept the error, returning a partial disassembly."
          disassembly))
      disassembly)))

(defun strings (disassembly)
  (with-accessors ((instructions disassembly-instructions)
                   (instruction-count disassembly-instruction-count))
      disassembly
    (loop for i from 0 to (1- instruction-count)
       for instruction = (aref instructions i)
       when (eql (instruction-mnemonic instruction) 'out)
       collect (loop for j upfrom i
                  for inst = (aref instructions j)
                  until (cl:not (eql (instruction-mnemonic inst) 'out))
                  for operand = (first (instruction-operands inst))
                  when (typep operand 'literal)
                  collect (code-char (literal-value operand)) into chars
                  finally (progn (setf i j)
                                 (return (coerce chars 'string)))))))

(defclass reference ()
  ((from-address
    :type (unsigned-byte 16)
    :initarg :from
    :reader reference-from-address
    :documentation "Address where the reference occurs.")
   (to-address
    :type (unsigned-byte 16)
    :initarg :to
    :reader reference-to-address
    :documentation "Address referred to."))
  (:documentation "Base class for references within disassembled code."))

(defclass code-reference (reference)
  ()
  (:documentation ""))

(defclass function-reference (code-reference)
  ()
  (:documentation ""))

(defclass data-reference (reference)
  ()
  (:documentation ""))

(defgeneric instruction-code-references (instruction)
  (:documentation "Return a list of instruction references for a given instruction.")
  (:method ((instruction instruction))
    (with-accessors ((location location)
                     (mnemonic instruction-mnemonic)
                     (operands instruction-operands))
        instruction
      (case mnemonic
        (call (let ((addr (first operands)))
                (when (typep addr 'literal)
                  (make-instance 'function-reference
                                 :from-address location
                                 :to-address (literal-value addr)))))
        (ret (let ((addr (first operands)))
               (when (typep addr 'literal)
                 (make-instance 'code-reference
                                :from-address location
                                :to-address (literal-value addr)))))
        (jf (let ((addr (second operands)))
              (when (typep addr 'literal)
                (make-instance 'code-reference
                               :from-address location
                               :to-address (literal-value addr)))))
        (jt (let ((addr (second operands)))
              (when (typep addr 'literal)
                (make-instance 'code-reference
                               :from-address location
                               :to-address (literal-value addr)))))
        (t nil)))))

(defgeneric instruction-data-references (instruction)
  (:documentation "Return a list of data references for a given instruction.")
  (:method ((instruction instruction))
    (with-accessors ((location location)
                     (mnemonic instruction-mnemonic)
                     (operands instruction-operands))
        instruction
      (case mnemonic
        (rmem (let ((addr (second operands)))
                (when (typep addr 'literal)
                  (make-instance 'data-reference
                                 :from-address location
                                 :to-address (literal-value addr)))))
        (wmem (let ((addr (second operands)))
                (when (typep addr 'literal)
                  (make-instance 'data-reference
                                 :from-address location
                                 :to-address (literal-value addr)))))
        (t nil)))))

(defun disassemble-all (bytes)
  (flet ((skip (c)
           (declare (ignore c))
           (when (find-restart 'skip)
             (invoke-restart 'skip))))
    (handler-bind ((invalid-opcode #'skip))
      (disassemble bytes))))

#+nil
(progn
  (defun skip (c)
    (declare (ignore c))
    (let ((skip-restart (find-restart 'skip)))
      (when skip-restart
        (invoke-restart skip-restart))))

  (handler-bind ((invalid-opcode #'skip))
    (disassemble (virtual-machine-memory *current-state*))))

#+nil
(with-open-file (f #p"out" :direction :output
                   :if-exists :overwrite
                   :if-does-not-exist :create)
  (loop for inst across (disassembly-instructions *dis*)
   do (print inst f)))
