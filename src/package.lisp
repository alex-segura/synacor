(defpackage #:synacor
  (:use #:cl)
  (:shadow #:step
           #:set
           #:push
           #:pop
           #:eq
           #:mod
           #:and
           #:or
           #:not
           #:load
           #:disassemble)
  (:export
   ;; conditions
   #:virtual-machine-condition
   #:virtual-machine-error
   #:stack-underflow
   #:halt
   #:illegal-instruction
   #:illegal-argument

   ;; virtual machine
   #:virtual-machine
   #:virtual-machine-registers
   #:virtual-machine-memory
   #:virtual-machine-ip
   #:make-virtual-machine
   #:with-virtual-machine
   #:register-value
   #:memory-value

   ;; commands
   #:save-state
   #:restore-state
   #:load
   #:step
   #:run

   ;; hooks
   #:*step-hook*
   #:*instruction-hook*

   ;; disassembly
   #:disassembled-object
   #:operand
   #:register
   #:literal
   #:data
   #:instruction
   #:disassembly-error
   #:invalid-opcode
   #:invalid-operand
   #:disassembly
   #:strings
   #:reference
   #:code-reference
   #:function-reference
   #:data-reference
   #:disassemble
   #:disassemble-all

   ;; debugging
   #:with-tracing
   #:breakpoint
   #:add-breakpoint
   #:remove-breakpoint
   #:list-breakpoints
   #:with-debugging
   #:print-stack
   #:print-memory))
