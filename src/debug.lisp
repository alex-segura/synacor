;;;; debug.lisp -- Debugging tools for the virtual machine

(in-package #:synacor)

(defun trace-instruction (instruction stream)
"Print INSTRUCTION to stream. Decodes and prints any arguments to INSTRUCTION."
  (format stream "~a" instruction)
  (loop for i upfrom 0
     repeat (instruction-arity instruction)
     do (progn
          (format stream " ")
          (let ((value (memory-value (+ *ip* i))))
            (cond ((literal-value-p value)
                   (format stream "#x~4,'0x" value))
                  ((register-p value)
                   (format stream "R:~d" (decode-register value)))))))
  (terpri stream)
  (force-output stream))

(defun trace-from (stream start &optional end)
  "Establish an *INSTRUCTION-HOOK* that traces instructions from START to
STREAM. If END is provided, tracing is disabled once execution hits END."
  (setf *instruction-hook*
        (Let ((old-hook *instruction-hook*)
              (should-trace-p nil))
          (lambda (name opcode)
            (declare (ignore opcode))
            (when (= *ip* start)
              (setf should-trace-p t))
            (when should-trace-p
              (trace-instruction name stream))
            (when (cl:and end (= *ip* end))
              (setf *instruction-hook* old-hook))))))

(defun make-trace-hook (stream)
  (lambda (name opcode)
    (declare (ignore opcode))
    (trace-instruction name stream)))

(defmacro with-tracing ((&key (stream *standard-output*)) &body body)
  "Enables tracing to STREAM within the dynamic context enclosing BODY."
  `(let ((*instruction-hook* (make-trace-hook ,stream)))
     ,@body))

(define-condition breakpoint (virtual-machine-condition)
  ()
  (:documentation
   "Signalled when a debug breakpoint is reached during VM execution.")
  (:report (lambda (condition stream)
             (let ((vm (virtual-machine-state condition)))
               (format stream
                     "Reached breakpoint at IP = #x~4,'0x."
                     (virtual-machine-ip vm))
               (terpri stream)
               (terpri stream)
               (format stream "Registers:~%~%")
               (describe vm stream)
               (terpri stream)
               (format stream "Stack:~%~%")
               (print-stack stream vm)))))

(defvar *breakpoints* nil
  "List of active breakpoints.")

(defvar *breakpoint* nil
  "The breakpoint that signalled the most recent break.")

(defun add-breakpoint (address &key test)
  (declare (type (unsigned-byte 16) address))
  (cl:push (cons address test) *breakpoints*))

(defun remove-breakpoint (address)
  (declare (type (unsigned-byte 16) address))
  (delete address *breakpoints* :test #'= :key #'car))

(defun list-breakpoints ()
  (mapcar #'car *breakpoints*))

(defun print-breakpoints (&optional (stream t))
  (format stream "~{#x~4,'0x~%~}" (list-breakpoints)))

(defun breakpoint ()
  (when (cl:not *breakpoint*)
    (setf *breakpoint* *ip*))
  (restart-case (signal 'breakpoint)
    (step ()
      :report "Step over the current instruction."
      (install-debug-step-hook))
    (next ()
      :report "Step to the next instruction."
      (install-debug-next-hook))
    (finish ()
      :report "Finish exuection of the current subroutine."
      (install-debug-finish-hook))
    (continue ()
      :report "Resume execution.")
    (remove ()
      :report "Remove this breakpoint and resume execution."
      (remove-breakpoint *breakpoint*)
      (setf *breakpoint* nil))))

(defun next-instruction-address ()
  (+ *ip* 1 (instruction-arity (current-instruction))))

(defun install-debug-step-hook ()
  (let ((old-hook *step-hook*))
    (setf *step-hook*
          (if (eql (current-instruction) 'call)
              (let ((next-instruction (next-instruction-address)))
                (lambda ()
                  (when (= *ip* next-instruction)
                    (setf *step-hook* old-hook)
                    (breakpoint))))
              (lambda ()
                (setf *step-hook* old-hook)
                (breakpoint))))))

(defun install-debug-next-hook ()
  (let ((old-hook *step-hook*))
    (setf *step-hook*
          (lambda ()
            (setf *step-hook* old-hook)
            (breakpoint)))))

(defun install-debug-finish-hook ()
  (let ((old-hook *step-hook*))
    (setf *step-hook*
          (lambda ()
            (when (eql (current-instruction) 'ret)
              (setf *step-hook*
                    (lambda ()
                      (setf *step-hook* old-hook)
                      (breakpoint))))))))

(defun check-breakpoints ()
  (let ((bp (find *ip* *breakpoints* :test #'= :key #'car)))
    (when bp
      (destructuring-bind (address . condition) bp
        (declare (ignore address))
        (if condition
            (when (funcall condition *vm*)
              (breakpoint))
            (breakpoint))))))

(defun call-with-debugging (f &key initial-breakpoints)
  (let ((*breakpoints* initial-breakpoints)
        (*step-hook* #'check-breakpoints))
    (handler-bind ((breakpoint #'invoke-debugger))
      (funcall f))))

(defmacro with-debugging ((&key initial-breakpoints) &body body)
  `(call-with-debugging (lambda () ,@body)
                        ,@(when initial-breakpoints
                            `(:initial-breakpoints ,initial-breakpoints))))

(defun disassemble-next (&key (vm *vm*) (count 24) (stream t))
  "Dissasemble the next N bytes."
  (with-virtual-machine (vm)
    (with-accessors ((instructions disassembly-instructions))
        (disassemble (subseq *memory* *ip* (+ *ip* count)))
      (loop for instruction across instructions
         do (pprint instruction stream))
      (terpri stream))))

(defun print-memory (&key (vm *vm*) (stream t) (group-by #x10) (start 0) end)
  (let ((end (- (cl:or end +memory-size+) group-by)))
    (with-virtual-machine (vm)
      (loop for i from start to end by group-by
         do (progn (format stream "~4,'0x: " i)
                   (loop for j from 0 to (1- group-by)
                      for v = (memory-value (+ i j))
                      do (format stream "~4,'0x " v))
                   (format stream "~c" #\Space)
                   (loop for j from 0 to (1- group-by)
                      for v = (memory-value (+ i j))
                      do (if (cl:and (>= v #x20) (<= v #x7e))
                             (format stream "~c" (code-char v))
                             (format stream "~c" #\Period)))
                   (terpri stream))))))

(defun print-stack (&optional (stream t) (*vm* *vm*))
  (format stream "~4,'0x <--- [SP]~%" (first *stack*))
  (dolist (elt (rest *stack*))
    (format stream "~4,'0x~%" elt)))
