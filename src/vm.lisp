;;;; vm.lisp -- virtual machine definitions

(in-package #:synacor)

(defconstant +memory-size+ #.(ash 1 15))

(defun make-memory ()
  (make-array (list +memory-size+)
              :element-type '(unsigned-byte 16)
              :initial-element #x0000))

(defconstant +number-of-registers+ 8)

(defun make-registers ()
  (make-array (list +number-of-registers+)
              :element-type '(unsigned-byte 16)
              :initial-element #x0000))

(defun make-stack () nil)

(deftype memory ()
  `(simple-array (unsigned-byte 16) (,+memory-size+)))

(deftype registers ()
  `(simple-array (unsigned-byte 16) (,+number-of-registers+)))

(defstruct virtual-machine
  (memory    (make-memory)    :type memory)
  (registers (make-registers) :type registers)
  (stack     (make-stack)     :type list)
  (ip        #x0000           :type (unsigned-byte 16)))

(defmethod describe-object ((vm virtual-machine) stream)
  (with-slots (registers stack ip) vm
    (format stream
            "IP: 0x~4,'0x~%~:{R~d: 0x~4,'0x~%~}"
            ip
            (loop for r in (coerce registers 'list)
               for i upfrom 0
               collect `(,i ,r)))))

(defvar *vm*)

(defvar *vm-output-stream* *standard-output*)

(define-symbol-macro *memory* (virtual-machine-memory *vm*))

(define-symbol-macro *registers* (virtual-machine-registers *vm*))

(define-symbol-macro *stack* (virtual-machine-stack *vm*))

(define-symbol-macro *ip* (virtual-machine-ip *vm*))

(defun call-with-virtual-machine (f vm)
  (let ((*vm* vm))
    (funcall f)))

(defmacro with-virtual-machine ((&optional vm) &body body)
  `(call-with-virtual-machine (lambda () ,@body)
                              ,(if vm vm '(make-virtual-machine))))

(declaim (inline register-value))
(defun register-value (register &optional (*vm* *vm*))
  "Retrieve the value of REGISTER from the virtual machine."
  (aref *registers* register))

(declaim (inline (setf register-value)))
(defun (setf register-value) (value register &optional (*vm* *vm*))
  (declare (type (unsigned-byte 16) register value))
  (setf (aref *registers* register) value))

(declaim (inline memory-value))
(defun memory-value (address &optional (*vm* *vm*))
  "Retrieve the value at ADDRESS within the virtual machine's memory."
  (declare (type (unsigned-byte 16) address))
  (aref *memory* address))

(declaim (inline (setf memory-value)))
(defun (setf memory-value) (value address &optional (*vm* *vm*))
  (declare (type (unsigned-byte 16) address value))
  (setf (aref *memory* address) value))

(declaim (inline decode-register))
(defun decode-register (word)
  "For an encoded register operand WORD, compute the offset into the registers array."
  (unless (register-p word)
    (error 'illegal-argument
           :type :register
           :operand word))
  (logand #x0007 word))

(defun save-state (file)
  (with-open-file (f file
                     :element-type '(unsigned-byte 16)
                     :direction :output
                     :if-exists :supersede)
    (write-byte *ip* f)
    (dotimes (i +number-of-registers+)
      (write-byte (register-value i) f))
    (write-byte (length *stack*) f)
    (dolist (b *stack*)
      (write-byte b f))
    (dotimes (i +memory-size+)
      (write-byte (memory-value i) f))))

(defun restore-state (file &optional (*vm* *vm*))
  (with-open-file (f file :element-type '(unsigned-byte 16))
    (setf *ip* (read-byte f))
    (dotimes (i +number-of-registers+)
      (setf (register-value i) (read-byte f)))
    (let ((stack-size (read-byte f))
          (stack))
      (dotimes (i stack-size)
        (cl:push (read-byte f) stack))
      (setf *stack* (reverse stack)))
    (dotimes (i +memory-size+)
      (setf (memory-value i) (read-byte f)))))

(defun load (file)
  (with-open-file (f file :element-type '(unsigned-byte 16))
    (handler-case
        (loop for word = (read-byte f)
           for i upfrom 0
           do (setf (aref *memory* i) word))
      (end-of-file (e)
        (declare (ignore e))))
    (values)))

(defun reset (&key soft)
  (unless soft
    (dotimes (i +memory-size+)
      (setf (aref *memory* i) #x0000)))
  (dotimes (i +number-of-registers+)
    (setf (aref *registers* i) #x0000))
  (setf *stack* nil)
  (setf *ip* #x0000))

(defconstant +opcode-table-size+ 22)

(defvar *opcode-table* (make-array (list +opcode-table-size+)))

(defun current-instruction (&optional (vm *vm*))
  (with-slots (memory ip) vm
    (opcode-mnemonic (aref memory ip))))

(defun decode-instruction (opcode)
  (fdefinition (opcode-mnemonic opcode)))

(declaim (inline literal-value-p))
(defun literal-value-p (word)
  (declare (type (unsigned-byte 16) word))
  (the boolean (<= #x0000 word #x7fff)))

(declaim (inline register-p))
(defun register-p (word)
  (declare (type (unsigned-byte 16) word))
  (the boolean (<= #x8000 word #x8007)))

(defun fetch-value (word)
  (declare (type (unsigned-byte 16) word))
  (cond ((register-p word)
         (register-value (decode-register word)))
        ((literal-value-p word) word)
        (t (error 'illegal-argument
                  :operand word
                  :as :value))))

(defun fetch (&key as)
  (prog1
      (restart-case
          (let ((word (memory-value *ip*)))
            (if (null as)
                (the (unsigned-byte 16) word)
                (ecase as
                  (:register (decode-register word))
                  (:value (the (unsigned-byte 16)
                               (fetch-value word)))
                  (:instruction (decode-instruction word)))))
        (use-value (value)
          :report "Specify a value to use this time."
          value)
        (store-value (value)
          :report "Specify a value to store and use in the future."
          (setf (memory-value *ip*) value)))
    (incf *ip*)))

(defvar *step-hook* nil)

(defun step ()
  (when *step-hook* (funcall *step-hook*))
  (let ((inst (fetch :as :instruction)))
    (funcall inst)))

(defun run ()
  (handler-case
      (restart-case
          (loop (step))
        (reset ()
          :report "Reinitialize the machine state."
          (progn (reset :soft t)
                 (run)))
        (halt ()
          :report "Halt execution of the machine."
          (signal 'halt)))
    (halt (c)
      (declare (ignore c))
      (values))))
