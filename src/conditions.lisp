;;;; conditions.lisp -- condition and error definitions for the VM

(in-package #:synacor)

(define-condition virtual-machine-condition (simple-condition)
  ((state
    :initarg :state
    :reader virtual-machine-state
    :documentation "State of the virtual machine at the point of signal"))
  (:default-initargs :state *vm*)
  (:documentation "Base condition for signals raised by the VM"))

(define-condition halt (virtual-machine-condition)
  ()
  (:documentation "Signalled when the machine reaches a HALT instruction")
  (:report (lambda (condition stream)
             (declare (ignore condition))
             (format stream "VM halted."))))

(define-condition virtual-machine-error (virtual-machine-condition simple-error)
  ()
  (:documentation "Base condition for errors raised by the VM"))

(define-condition illegal-instruction (virtual-machine-error)
  ((word
    :type (unsigned-byte 16)
    :initarg :word
    :reader illegal-instruction-word
    :documentation "Word that failed to be parsed as an instruction"))
  (:documentation "Signalled when an attempt is made to decode a word as an instruction
but that word does not correspond to any valid instruction")
  (:report (lambda (condition stream)
             (format stream
                     "Illegal instruction: #x~4,'0x (IP: #x~4,'0x)."
                     (illegal-instruction-word condition)
                     (virtual-machine-ip (virtual-machine-state condition))))))

(define-condition stack-underflow (virtual-machine-error)
  ()
  (:documentation "Signalled when a POP is executed on an empty VM stack")
  (:report (lambda (condition stream)
             (format stream
                     "Stack underflow (IP: #x~4,'0x)."
                     (virtual-machine-ip (virtual-machine-state condition))))))

(define-condition illegal-argument (virtual-machine-error)
  ((operand
    :type (unsigned-byte 16)
    :initarg :operand
    :reader illegal-argument-operand)
   (type
    :initform nil
    :initarg :type
    :reader illegal-argument-type))
  (:documentation "")
  (:report (lambda (condition stream)
             (let ((state (virtual-machine-state condition)))
               (format stream
                       "Illegal arugment #x~4,'0x (as ~a) in instruction ~a (IP: #x~4,'0x)."
                       (illegal-argument-operand condition)
                       (illegal-argument-type condition)
                       (current-instruction state)
                       (virtual-machine-ip state))))))
