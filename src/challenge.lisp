(in-package #:synacor)

(defparameter *program* #p"./challenge.bin")
(defparameter *state* #p"./state")

(defun run-challenge ()
  (with-virtual-machine ()
    (with-debugging (:initial-breakpoints '(#x0ad0))
      (if (uiop:file-exists-p *state*)
          (restore-state *state*)
          (load *program*))
      (run))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Teleporter (see notes.org)

(declaim (optimize (speed 3)))

(defun confirm (r7)
  ;; Probably not the fastest (takes several minutes to complete)
  ;; But the tail-recursion, inlining, and memoization are enough to get
  ;; through.
  ;;
  ;; My initial hope was to define compiler macros for +-16bit and see if SBCL
  ;; propagate the constants through, as it unrolled things during the inlining,
  ;; but that didn't seem to work.
  (declare (type (unsigned-byte 16) r7))
  (let ((memo (make-hash-table :test #'equal)))
    (labels ((recur (r0 r1 r7 k)
               (declare (type (unsigned-byte 16) r0 r1 r7))
               (multiple-value-bind (value foundp)
                   (gethash (list r0 r1) memo)
                 (if foundp
                     (funcall k value)
                     (cond ((= r0 #x0000)
                            (let ((r (+-16bit r1 #x0001)))
                              (setf (gethash (list r0 r1) memo) r)
                              (funcall k r)))
                           ((= r1 #x0000)
                            (let ((x (+-16bit r0 #x7fff)))
                              (recur x
                                     r7
                                     r7
                                     (lambda (r)
                                       (setf (gethash (list x r7) memo) r)
                                       (funcall k r)))))
                           (t
                            (let ((x (+-16bit r1 #x7fff)))
                              (recur r0
                                     x
                                     r7
                                     (lambda (r)
                                       (setf (gethash (list r0 x) memo) r)
                                       (let ((y (+-16bit r0 #x7fff)))
                                         (recur y
                                                r
                                                r7
                                                (lambda (w)
                                                  (setf (gethash (list y r) memo) w)
                                                  (funcall k w)))))))))))))
      (declare (inline recur))
      (the (unsigned-byte 16)
           (recur 4 1 r7 #'identity)))))

(defun solve ()
  (loop for i from 0 to #x8000
     for r = (confirm i)
     when (= r 6)
     return i))

;; (solve) => 25734

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Orb (see notes.org)

;; "journal" :; #x564e
;; journal description :: #x5656

(defun find-addresses (value)
  (declare (type (unsigned-byte 16) value))
  (loop
     for word across *memory*
     for address upfrom 0
     when (= word value)
     collect address))

#+nil
(defvar *twenty-twos* (find-addresses 22)) ; => (#x0F70 #x1238 #x6B28)

;; #x0f70 is a likely place to find the orb weight. It's referenced in
;; code called by the special room functions on grid rooms.

(defun orb-weight ()
  (memory-value #x0f70))

(defun (setf orb-weight) (value)
  (setf (memory-value #x0f70) value))
